'use strict'

const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

let div = document.createElement('div');
div.id = 'root';
var ul = document.createElement('ul');


function getBooks(book) {
    let li = document.createElement('li');
    li.append(`Aвтор : "${book.author}"` + ` Назва: "${book.name}"` + ` Цiна: ${book.price}`);
    return li;
}

for (let book of books) {
    try {
        if (typeof book.author === 'undefined') {
            throw "Author is missing"
        } else if (typeof book.name === 'undefined') {
            throw "Name is missing"
        } else if (typeof book.price === 'undefined') {
            throw "Price is missing"
        }
        ul.append(getBooks(book));
    } catch (e) {
        console.log(e)
    }
}

document.querySelector('body').prepend(div);
document.querySelector('div').prepend(ul);







